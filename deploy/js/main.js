'use strict';

~ (function() {
    var $ = TweenMax,
        updateBar,
        ad = document.getElementById('mainContent'),
        myVideo = document.getElementById('myVideo'),       

        playPauseBtn = document.getElementById('playPause'),
        muteUnmuteBtn = document.getElementById('muteUnmute'),
        barSize=600,
        bar=document.getElementById('defaultBar'),
	    progressBar=document.getElementById('progressBar'),
        replayBtn = document.getElementById('replay');



    window.init = function() {
        allEventHandlers();
    }

    function allEventHandlers() {
    bar.addEventListener('click', clickedBar, false);    
    playPauseBtn.addEventListener('click', playPauseBtnEventHandler, false);
    muteUnmuteBtn.addEventListener('click', muteUnmuteBtnEventHandler, false);
    replayBtn.addEventListener('click', replayBtnEventHandler, false);    
    myVideo.addEventListener('click', goToendFrame, false);    
    }

    function playPauseBtnEventHandler(){
        myVideo.muted = true;    
        progressBar.style.width='0';
        defaultBar.style.visibility = 'visible';
        playPauseBtn.style.visibility = 'visible';
        muteUnmuteBtn.style.visibility = 'visible';
        bar.addEventListener('click', clickedBar, false);

        
        if( myVideo.paused ) {            
            myVideo.play();
            playPauseBtn.classList.toggle('pause');         
            updateBar= setInterval(update, 500);
        }
        else {
            myVideo.pause();
            playPauseBtn.classList.toggle('pause');           
            updateBar= setInterval(update, 500);
        }
    }

    function muteUnmuteBtnEventHandler(){

        if( myVideo.muted ) {    
            myVideo.muted = false;    
            muteUnmuteBtn.classList.toggle('unmute');         
         }

        else {         
            myVideo.muted = true;    
            muteUnmuteBtn.classList.toggle('unmute');         
        }
    }

    function update() {
        if (!myVideo.ended) {
            var size=parseInt(myVideo.currentTime*barSize/myVideo.duration);
            progressBar.style.width=size+'px';
            
        } else {
            defaultBar.style.visibility = 'hidden';
            playPauseBtn.style.visibility = 'hidden';
            muteUnmuteBtn.style.visibility = 'hidden';
            bar.removeEventListener('click', clickedBar, false);
            
        }
    }
    
    function clickedBar(e){
            var mouseX=e.pageX-bar.offsetLeft;
            var newtime=mouseX*myVideo.duration/barSize;
            myVideo.currentTime=newtime;
            progressBar.style.width=mouseX+'px';
    }

    function replayBtnEventHandler(){
        myVideo.pause();       
        myVideo.currentTime = 0;       
        playPauseBtn.className ='play';       
        muteUnmuteBtn.className ='mute';       
        playPauseBtnEventHandler();
        
    }

    function goToendFrame(){
        myVideo.currentTime = (this.duration);
        playPauseBtn.style.visibility = 'hidden';
        muteUnmuteBtn.style.visibility = 'hidden';
        defaultBar.style.visibility = 'hidden';
      

    }





}) ();

window.onload = init();